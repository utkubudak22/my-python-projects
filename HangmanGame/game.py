"""
@author: utku
@date: 11.03.2021
@desc: This is a simple python script to demonstrate a game
called Hangman!
"""

import hangman_art
import words
import random

word_list = []  # empty list of the chars of the word
word_str = ""   # conversion of word_list list into string
word_check = []  # False -> if the char does not exist in string, True -> if the char exists in string
life_check = 6  # The user can mostly guess 6 wrong characters!
game_lose = False  # When this turns to True : Game Over!
chosen_letter = []  # List of the words that the user has already picked before.

print(hangman_art.logo)
print("\n")
print("Welcome to the game Hangman!\n")
print("Your word is randomly selected!\n")

chosen_word = random.choice(words.word_list)
len_word = len(chosen_word)

for i in range(1, len_word +1):
    word_list.append("_")

word_str = ' '.join([str(elem) for elem in word_list])
print(word_str + "\n")
print(hangman_art.stages[life_check])

while word_str.find("_") != -1:
    print("You have already chosen these words: ", chosen_letter)
    user_guess = input("Please make a guess! ").lower()
    chosen_letter.append(user_guess)

    for i in chosen_word:
        if user_guess == i:
            word_check.append("True")
        elif user_guess != i:
            word_check.append("False")

    i = 0
    while i <= len_word -1:
        if word_check[i] == "True":
            word_list[i] = user_guess
        elif word_check[i] != "False":
            pass
        i = i + 1

    word_check_str = ' '.join([str(elem) for elem in word_check])
    if word_check_str.find("True") == -1:
        life_check = life_check - 1
        print("You made a wrong guess!")
        print(hangman_art.stages[life_check])

    if life_check == 0:
        print("You lost the game. Please restart the game to try again!")
        game_lose = True
        break
    word_check = []

    word_str = ' '.join([str(elem) for elem in word_list])
    print(word_str)

if not game_lose:
    print("-------------------")
    print("You won the game!!!")






