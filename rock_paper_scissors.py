"""
@author: utku
@date: 10.03.2021
@desc: This is a simple python script to demonstrate a game
called rock, paper, scissors. The code size can be reduced with better
algorithms!
"""

import random

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

cpu_selection = random.randint(1, 3)

if cpu_selection == 1:
    cpu_selection = rock
elif cpu_selection == 2:
    cpu_selection = paper
elif cpu_selection == 3:
    cpu_selection = scissors

user_selection = input("Please write: Rock, Paper or Scissors! ")

if user_selection == "Rock" or user_selection == "rock":
    print("You selected " + user_selection + "\n")
    print(rock + "\n")
    if cpu_selection == rock:
        print("CPU selected Rock" + "\n")
        print(rock + "\n")
        print("Draw" + "\n")
    elif cpu_selection == paper:
        print("CPU selected Paper" + "\n")
        print(paper + "\n")
        print("You lost" + "\n")
    elif cpu_selection == scissors:
        print("CPU selected Scissors" + "\n")
        print(scissors + "\n")
        print("You won" + "\n")

elif user_selection == "Paper" or user_selection == "paper":
    print("You selected " + user_selection + "\n")
    print(paper + "\n")
    if cpu_selection == rock:
        print("CPU selected Rock" + "\n")
        print(rock + "\n")
        print("You won" + "\n")
    elif cpu_selection == paper:
        print("CPU selected Paper" + "\n")
        print(paper + "\n")
        print("Draw" + "\n")
    elif cpu_selection == scissors:
        print("CPU selected Scissors" + "\n")
        print(scissors + "\n")
        print("You lost" + "\n")

elif user_selection == "Scissors" or user_selection == "scissors":
    print("You selected " + user_selection + "\n")
    print(scissors + "\n")
    if cpu_selection == rock:
        print("CPU selected Rock" + "\n")
        print(rock + "\n")
        print("You lost" + "\n")
    elif cpu_selection == paper:
        print("CPU selected Paper" + "\n")
        print(paper + "\n")
        print("You won" + "\n")
    elif cpu_selection == scissors:
        print("CPU selected Scissors" + "\n")
        print(scissors + "\n")
        print("Draw" + "\n")
