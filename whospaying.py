"""
@author: utku
@date: 10.03.2021
@desc: This is a simple python script that picks randomly who is
gonna pay the food bill from the given input!
"""
import random

names = input("Give names separated with comma and space! (utku, ege, ahmet)\n")

# Divide the input that is separated with comma and save into a list.
person_list = names.split(", ")
# Calculate the length of a list.
len_list = len(person_list)
# Find a random value between 1 and max length of the list
rand_number = random.randint(0, len_list-1)

print(person_list[rand_number] + " is gonna pay the bill!")
