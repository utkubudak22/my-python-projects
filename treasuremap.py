"""
@author: utku
@date: 10.03.2021
@desc: This is a simple python script to demonstrate a game
called treasure map, chose a column and a row as 23: column->2, row->3
"""

row1 = ["⬜", "⬜", "⬜"]
row2 = ["⬜", "⬜", "⬜"]
row3 = ["⬜", "⬜", "⬜"]
map_user = [row1, row2, row3]

print(f"{row1}\n{row2}\n{row3}")
position = input("Where do you want to put the treasure? ")

temp_position = list(position)
column_selection = int(temp_position[0])
row_selection = int(temp_position[1])

map_user[row_selection-1][column_selection-1] = "X"

row1 = map_user[0]
row2 = map_user[1]
row3 = map_user[2]

print(f"{row1}\n{row2}\n{row3}")