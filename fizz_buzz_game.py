"""
@author: utku
@date: 10.03.2021
@desc: This is a simple python script to demonstrate a game
called FizzBuzz.
If a number is divisible by 3 -> Fizz
If a number is divisible by 5 -> Buzz
If a number is divisible by both 3 and 5 -> FizzBuzz
"""

for number in range(1, 101):
    if (number % 3) == 0 and (number % 5) != 0:
        print("Fizz")
    elif (number % 5) == 0 and (number % 3) != 0:
        print("Buzz")
    elif (number % 3) == 0 and (number % 5) == 0:
        print("FizzBuzz")
    else:
        print(number)
